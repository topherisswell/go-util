package goutil

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// SleepWithContext will block until a specified duration has passed
// or until the context is canceled, if the context is canceled before the duration
// has elapsed, it will return with an error.
func SleepWithContext(d time.Duration, ctx context.Context) (err error) {
	timer := time.NewTimer(d)

	select {
	case <-timer.C:
		// duration has been reached. Return with no error.
		return nil
	case <-ctx.Done():
		// context canceled before duration reached. Return with error.
		return ctx.Err()
	}
}

// beatHeart is a simple function to attempt to send a empty struct through
// a channel in a non-blocking way. Used for sending heartbeats.
func BeatHeart(heartbeat chan<- interface{}) {
	// TODO - hooks for incrementing prometheus counter
	//heartbeatCounter.Inc()
	select {
	case heartbeat <- struct{}{}:

	default: // sing like nobody's listening, and move on if no one hears your heartbeat
	}
}

// CancelOnSignal takes a context and returns a child of that context.
// It listens for OS signals (SIGTERM, SIGINT, etc) and cancels
// the returned context when an OS signal is received.
// used for handling Ctl-C or other interrupts signalling your app should exit
func CancelOnSignal(ctx context.Context) context.Context {
	var cctx, canx = context.WithCancel(ctx)
	cancelChan := make(chan os.Signal, 1)
	signal.Notify(cancelChan, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		select {
		case <-ctx.Done():
			// parent context is canceled
		case <-cancelChan:
			// received OS signal
		case <-cctx.Done():
			// child context is canceled (I don't know how/why)
		}
		canx()
	}()
	return cctx
}
