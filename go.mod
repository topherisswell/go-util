module gitlab.com/topherisswell/go-util

go 1.18

require (
	github.com/go-logr/logr v1.2.3
	github.com/google/uuid v1.3.0
)
