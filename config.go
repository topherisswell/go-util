package goutil

import (
	"time"

	"gitlab.com/topherisswell/go-util/meatlog"
)

// Config struct stores global runtime configuration items that are usually
// determined at starttime as command-line flags, config file parameters, or environment variables.
// designed to be fairly agnostic to specific program use and cover options that affect 90%+ of programs
type Config struct {
	// This is the maximum amount of time any remote API call should wait for response before cancelling.
	// 10 seconds is probably a safe value here. Slow API calls will use the full timeout length.
	// other API calls may use a percentage of this value (i.e APITimeout / 2 or 3) or a static value <= to this,
	// but NO remote API call should wait _longer_ than this value.
	// Increase this if it operates in a lossy or high-latency network.
	MaxAPITimeout time.Duration
	// This is how often a long-running routine/service/thread is to report healthy to
	// its watchdog. This should be less than or equal to  MaxAPITimeout / 8 in order to ensure that
	// anything that kills children after that amount of time has time to receive at least 2 heartbeats
	// 500ms is usually a safe value, assuming your MaxAPITimeout is at least 4s.
	HeartbeatInterval time.Duration

	// The name of this program. Used in logs and traces mostly. Easy to rebrand or
	// have multiple instances with different names to be able to differentiate them in the logs.
	AppName string
	// This is the version of the application. This is usually only used to include version information in the logs
	// Can be set by build flags in CI.
	AppVersion string

	// This is the logger we're configured to write to
	Logger meatlog.Meatlogger

	// Output File Path for the profiler to write to
	// When this is defined, the profile is enabled
	// This may affect performance.
	ProfilerOutput string

	// This is a flag that is assumed to be false unless marked otherwise at the command-line.
	// tldr: wrap your debugging / testing code in an InDevelopment test so that it never accidentially makes it to prod.
	// Any dev functionality should check this variable, and if it is production, any development functionality that shouldn't make it
	// into production should cause a panic instead of implementing the functionality. Examples include dumping requests / passwords or
	// any sensitive data into logs. A production application can be running in debug mode, but that should never put sensitive information
	// into logs. A development logger could log sensitive things if this variable is true.
	// This is stylized as "InDevelopment" instead of "InProduction" because Bools will default to false, and for safety, we want
	// to always assume we're in production unless we explicitly state otherwise. Could also be thought of as "AllowDiceyThings"
	// Setting this variable to true doesn't mean you're running code from a development branch, it means that you're running in a
	// development environment. Where it's safer to do things like blatantly trust input without validation or write passwords to STDOUT
	// or bypass authentication or print hints or continue execution despite failing state assertions.
	// When this is set to True, it should not have access to production databases nor should it be
	// sent production traffic. It should be perfectly safe to run this in a development environment with this set to False as well.
	// Finally, This is a guardrail and shouldn't be relied on alone to ensure that unsafe code doesn't make it to production,
	// do your best to remove your hacky BS before cutting a release.
	InDevelopment bool

	// This controls how the program handles tiny validation errors. Without StrictMode, an invalid, but unneeded parameter
	// might be discarded or filled with a default value in order to complete the request when possible. With StrictMode,
	// an invalid parameter will cause a request to fail, regardless of whether recovery was possible. Potentially enhances
	// security and encourages consistent, good practice with regard to API hygeine.
	// Commonly, StrictMode will mean turning Warnings into Errors. StrictMode should always be set in production (in my opinion).
	StrictMode bool
}

func DefaultConfig() *Config {
	var cfg Config = Config{
		MaxAPITimeout:     6 * time.Second,
		HeartbeatInterval: 500 * time.Millisecond,
		Logger:            meatlog.Default(),
		ProfilerOutput:    "",
		InDevelopment:     false,
		StrictMode:        false,
	}
	return &cfg
}

func DefaultConfigWithParams(appName string, appVersion string) *Config {
	var cfg = DefaultConfig()
	cfg.AppName = appName
	cfg.AppVersion = appVersion
	return cfg
}
