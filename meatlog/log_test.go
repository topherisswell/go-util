// defines the Meatlog type and interface for a more ergonomic logging solution
package meatlog

import (
	"testing"
)

func TestMeatlog_logmessages(t *testing.T) {
	logger := Default()
	logger.SetFlags(logger.Flags() | FLAG_LSHORTFILE)
	logger.Trace("this is a trace")
	logger.Debug("this is a debug")
	logger.Info("this is an info", 4, `u`)
	logger.Warn("this is a warning")
	//logger.Error("this is a error")
	//logger.Fatal("this is a fatal")
	logger.Tracef("this is a %s", "tracef")
	logger.Debugf("this is a %s", "debubg")
	logger.Infof("this is an %s", "infof")
	logger.Warnf("this is a %s", "warningf")
	//logger.Errorf("this is a %s", "errorf")
	//logger.Fatalf("this is a %s", "fatalf")
	logger.PrintMessage(13, 3, " element ", "message")
	logger.PrintfMessage(9, "%s message", "printf")
	logger.printMessageWithCallerDepth(1, 9, "one caller depth")
	logger.printMessageWithCallerDepth(2, 9, "two caller depth")
	logger.printMessageWithCallerDepth(3, 9, "three caller depth")
}
