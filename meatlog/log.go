// defines the Meatlog type and interface for a more ergonomic logging solution
package meatlog

import (
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"sync"
)

const (
	// Log Severity is a number between 1-24 representing how important the message is
	// 1 is the lowest severity, representing detailed information about a specific happening for highly surgical troubleshooting/analytics
	// 24 is the highest severity, representing the worse kind of error that must get everyone's attention. Generally accompanied by panicking the application
	// this corresponds to the open-telemetry standard
	// 1-4    TRACE  A fine-grained debugging event. Typically disabled in default configurations.
	TRACE_SEVERITY uint8 = 1
	// 5-8    DEBUG  A debugging event.
	DEBUG_SEVERITY uint8 = 5
	// 9-12   INFO   An informational event. Indicates that an event happened.
	INFO_SEVERITY uint8 = 9
	// 13-16  WARN   A warning event. Not an error but is likely more important than an informational event.
	WARN_SEVERITY uint8 = 13
	// 17-20  ERROR  An error event. Something went wrong.
	ERROR_SEVERITY uint8 = 17
	// 21-24  FATAL  A fatal error such as application or system crash.
	FATAL_SEVERITY uint8 = 21
	// The maximum severity we expect to handle
	// this is still considered FATAL for most intents and purposes
	// Anything above this should be ignored as invalid.
	MAX_SEVERITY uint8 = 24

	// Default settings for new Meatloggers

	// The default setting for new meatlogs, only messages of INFO or higher are logged.
	DEFAULT_MIN_SEVERITY uint8 = INFO_SEVERITY
	// The default setting for new meatlogs, Fatal severity messages will trigger a panic
	DEFAULT_PANIC_SEVERITY uint8 = FATAL_SEVERITY
	// The default setting for new meatlogs, set to force an exit only when the severity of a message
	// is higher than maximum (i.e. it never forces an exit)
	DEFAULT_EXIT_SEVERITY uint8 = MAX_SEVERITY + 1

	// These flags define which text to prefix to each log entry generated by the Logger.
	// Bits are or'ed together to control what's printed. With the exception of the Lmsgprefix flag,
	// there is no control over the order they appear (the order listed here) or the format they present (as described in the comments).
	// The prefix is followed by a colon only when Llongfile or Lshortfile is specified.
	FLAG_LDATE         = log.Ldate
	FLAG_LUTC          = log.LUTC
	FLAG_LLONGFILE     = log.Llongfile
	FLAG_LMICROSECONDS = log.Lmicroseconds
	FLAG_LMSGPREFIX    = log.Lmsgprefix
	FLAG_LSHORTFILE    = log.Lshortfile
	FLAG_LSTDFLAGS     = log.LstdFlags
	FLAG_LTIME         = log.Ltime
)

var (
	// Default prefixes for messages at given severity levels using the helper functions.

	tracePrefix = "TRACE "
	debugPrefix = "DEBUG "
	infoPrefix  = "INFO "
	warnPrefix  = "WARN "
	errorPrefix = "ERROR "
	fatalPrefix = "FATAL "
	panicPrefix = "PANIC "
)

type Meatlogger interface {
	IsDebug() bool
	AsStdLog() *log.Logger
	PrintMessage(severity uint8, args ...any)
	PrintfMessage(severity uint8, format string, args ...any)
	PrintlnMessage(severity uint8, args ...any)
	MinSeverityLevel() uint8
	SetMinSeverityLevel(severityLevel uint8)
	ExitSeverityLevel() uint8
	SetExitSeverityLevel(severityLevel uint8)
	PanicSeverityLevel() uint8
	SetPanicSeverityLevel(severityLevel uint8)
	Println(args ...any)
	Print(args ...any)
	Printf(format string, args ...any)
	Flags() int
	Prefix() string
	Output(calldepth int, s string) error
	SetOutput(w io.Writer)
	SetPrefix(s string)
	SetFlags(flags int)
	Writer() io.Writer
	Trace(args ...any)
	Tracef(format string, args ...any)
	Debug(args ...any)
	Debugf(format string, args ...any)
	Info(args ...any)
	Infof(format string, args ...any)
	Warn(args ...any)
	Warnf(format string, args ...any)
	Error(args ...any)
	Errorf(format string, args ...any)
	Fatal(args ...any)
	Fatalf(format string, args ...any)
	Fatalln(args ...any)
	Panic(args ...any)
	Panicf(format string, args ...any)
	Panicln(args ...any)
}
type Meatlog struct {
	logger *log.Logger // traditional logger struct for compatibility

	// minLogSeverityLevel is a number between 1-24 representing how important the message must be
	// to be sent to the log. 1 (low severity) means send all messages.
	// 24 (high severity) means only fatal message are sent.
	// this corresponds to the open-telemetry standard
	// 1-4    TRACE  A fine-grained debugging event. Typically disabled in default configurations.
	// 5-8    DEBUG  A debugging event.
	// 9-12   INFO   An informational event. Indicates that an event happened.
	// 13-16  WARN   A warning event. Not an error but is likely more important than an informational event.
	// 17-20  ERROR  An error event. Something went wrong.
	// 21-24  FATAL  A fatal error such as application or system crash.
	minLogSeverityLevel uint8

	// panicOnSeverityLevel is a number between 1-24 represent how severe a message must be to
	// call panic on your goroutine after logging the message. Setting this to 13 would exit on any warnings.
	// Setting it to 25 would ensure the logger never forces an exit (because 25 is an undefined logging level)
	// should (probably) default to 21 (FATAL messages only)
	panicOnSeverityLevel uint8

	// exitOnSeverityLevel is a number between 1-24 represent how severe a message must be to
	// force exit your program after logging the message. Setting this to 13 would exit on any warnings.
	// Setting it to 25 would ensure the logger never forces an exit (because 25 is an undefined logging level)
	// should (probably) default to 25 (no messages can force an hard exit)
	exitOnSeverityLevel uint8

	// this lock is for ensuring that only one message will log at a time, most of the time this isn't necessary
	// but in some cases we have to modify the logger object before making the call to the underlying go log library
	// in which case we don't want other callers logging while the logger object is in a temporary modified state.
	// a "Read" lock indicates a normal log message that should be totally safe concurrently with other normal log messages
	// a "Write" lock indicates that the logger is in a compromised state and no other log messages (normal or otherwise)
	// should be allowed to begin until this one is complete.
	lock *sync.RWMutex
}

// the standard / default meatlogger
var std = NewLogger(os.Stderr, "", log.LstdFlags, DEFAULT_MIN_SEVERITY, DEFAULT_PANIC_SEVERITY, DEFAULT_EXIT_SEVERITY)

// Default returns the default logger
func Default() *Meatlog {
	return std
}

// IsDebug returns true when the log level is set to debug or lower as the min logging level
func (l *Meatlog) IsDebug() bool {
	return l.minLogSeverityLevel < INFO_SEVERITY
}

// AsMeatLog() takes a log.Logger and returns a Meatlog with that logger embedded
// This makes the meatlog reference the actual log.Logger you send, so it should
// reflect any changes you make to that log.Logger after calling this function
func AsMeatLog(logger *log.Logger) *Meatlog {
	var locker sync.RWMutex

	var meaty Meatlog = Meatlog{
		logger:               logger,
		minLogSeverityLevel:  DEFAULT_MIN_SEVERITY,
		panicOnSeverityLevel: DEFAULT_PANIC_SEVERITY,
		exitOnSeverityLevel:  DEFAULT_EXIT_SEVERITY,
		lock:                 &locker,
	}
	return &meaty
}

// AsMeatLogWithParams() takes a log.Logger and returns a Meatlog with that logger embedded
// It also lets you configure the meatlog specific options by parameter
// This makes the meatlog reference the actual log.Logger you send, so it should
// reflect any changes you make to that log.Logger after calling this function
func AsMeatLogWithParams(logger *log.Logger, minLogSeverityLevel uint8, panicOnSeverityLevel uint8, exitOnSeverityLevel uint8) *Meatlog {
	var locker sync.RWMutex

	var meaty Meatlog = Meatlog{
		logger:               logger,
		minLogSeverityLevel:  minLogSeverityLevel,
		panicOnSeverityLevel: panicOnSeverityLevel,
		exitOnSeverityLevel:  exitOnSeverityLevel,
		lock:                 &locker,
	}
	return &meaty
}

// AsStdLog returns the underlying *log.Logger for compatibility with the go std library
func (l *Meatlog) AsStdLog() *log.Logger {
	return l.logger
}

// NewLogger generates a new logger object with the provided configuration
func NewLogger(out io.Writer, prefix string, flag int, minLogSeverityLevel uint8, panicOnSeverityLevel uint8, exitOnSeverityLevel uint8) *Meatlog {
	var logger = log.New(out, prefix, flag)
	var locker sync.RWMutex

	var meatlogger = Meatlog{
		logger:               logger,
		minLogSeverityLevel:  minLogSeverityLevel,
		panicOnSeverityLevel: panicOnSeverityLevel,
		exitOnSeverityLevel:  exitOnSeverityLevel,
		lock:                 &locker,
	}
	return &meatlogger

}

//// New makes a new golang built-in logger using the function signature of the golang built-in log module
//// This is only necessary if upstream go creates an interface for log.Logger
// func New(out io.Writer, prefix string, flag int) *log.Logger {
// 	var logger = log.New(out, prefix, flag)
// 	return logger
// }

// FormatCallerLocation() will concat the filename and line number
// filename will be whole path or single file name depending on the
// log flags. Will return an empty string if both flags are disabled
// SHORTFILE flag overrides the LONGFILE flag
// this expects to be passed the results of runtime.Caller() (after you check if OK)
func (l *Meatlog) FormatCallerLocation(file string, line int) string {
	if l.Flags()&(FLAG_LSHORTFILE|FLAG_LLONGFILE) != 0 {

		if l.Flags()&FLAG_LSHORTFILE != 0 {
			short := file
			for i := len(file) - 1; i > 0; i-- {
				if file[i] == '/' {
					short = file[i+1:]
					break
				}
			}
			file = short
		}
		return fmt.Sprintf("%s:%d: ", file, line)
	} else {
		return ""
	}
}

// PrintMessage sends a log message at a given severity
// this is designed as the external interface to directly print a message
// assumes a caller depth of 1
func (l *Meatlog) PrintMessage(severity uint8, args ...any) {
	l.printMessageWithCallerDepth(2, severity, args...)
}

// PrintMessageWithCallerDepth sends a log message at a given severity with a specified
// caller depth so we can get the file:line correctly. i.e. if this is called directly by
// interesting code, callerDepth would be 1, but if there is a helper wrapper around it
// like Warnf or something, then the callerDepth would be 2.
func (l *Meatlog) printMessageWithCallerDepth(callerDepth int, severity uint8, args ...any) {

	if l.minLogSeverityLevel <= severity {
		// This message is severe enough to log

		// if we have the SHORTFILE/LONGFILE flag set, we have to add a hack to ensure we get the file:line
		// of the CALLER of this function, otherwise the logger will always say the file:line of THIS
		// function, which is useless
		// the go builtin log library doesn't provide external options for controlling this
		if l.Flags()&(FLAG_LSHORTFILE|FLAG_LLONGFILE) != 0 {

			// remove flag before calling built-in log print functions so that it won't print the
			// file info, then we'll inject that information here and reenable the flag before returning
			// There is a race condition when two concurrent messages get logged and one of them
			// logs without file data at all. So we lock the meatlog, however, note that the underlying go log
			// calls will not respect this (i.e. meatlog.AsStdLog.Println() will ignore this lock and log anyway)
			// this won't cause panics/memory bugs/etc., so this should be fine, it just might make your log flags inconsistent
			// and the correct way to handle this is to not mix default logging unless necessary.

			// get the file information before we turn off the flags
			_, file, line, ok := runtime.Caller(callerDepth)
			if !ok {
				file = "???"
				line = 0
			}
			// this is the formatted log header of the file:line information (i.e. log.go:220)
			callerLocationString := l.FormatCallerLocation(file, line)
			// do a "write" lock, as we are changing flags and even normal log messages should pause until this is finished
			l.lock.Lock()
			// we'll need to set the flag back after the print function is called (so that the logger status is consistent)
			// so I don't think this defer won't be called if we panic or exit,
			// but that _should_ be okay as we'll be terminating the program and there shouldn't be any more logging after that.
			// if this proves NOT to be true, then we'll have to reset the flags and unlock in each of the os.exit and panic branches
			// and ensure teh l.lock.Unlock() doesn't get called twice (which is a panic :D)
			defer l.lock.Unlock()

			// turn off the flags, there's probably a fancier way involving bitwise operations that I'm too softbrained to think of right now
			if l.Flags()&FLAG_LSHORTFILE != 0 {
				l.SetFlags(l.Flags() - FLAG_LSHORTFILE)
				defer func() {
					l.SetFlags(l.Flags() | FLAG_LSHORTFILE)
				}()
			}
			if l.Flags()&FLAG_LLONGFILE != 0 {
				l.SetFlags(l.Flags() - FLAG_LLONGFILE)
				defer func() {
					l.SetFlags(l.Flags() | FLAG_LLONGFILE)
				}()
			}

			// now that we're safe to call the underlying go log methods, we need to inject the file information ourselves
			var newArgs []any
			newArgs = append(newArgs, callerLocationString)
			newArgs = append(newArgs, args...)
			args = newArgs

		} else {
			// if we don't have the SHORTFILE flag set, then we're not munging flags, so we don't need a "write" lock
			// attempt a blocking "read" lock. It is preferable for a log call to take longer than it is to fail to log at all
			l.lock.RLock()
			defer l.lock.RUnlock()
		}

		// actually make the log call to the underlying go log library
		l.logger.Print(args...)
		if l.exitOnSeverityLevel <= severity {
			// this message is severe enough to force an exit
			os.Exit(1)
		}
		if l.panicOnSeverityLevel <= severity {
			// this message is severe enough to force an panic
			panic("Panic called by logger due to message severity")
		}
	}
}

// PrintfMessage sends a formatted log message at a given severity
func (l *Meatlog) PrintfMessage(severity uint8, format string, args ...any) {
	l.printfMessageWithCallerDepth(2, severity, format, args...)
}

// printfMessageWithCallerDepth sends a formatted log message at a given severity and allows you to specify
// the caller depth (or LONGFILE/SHORTFILE flags to print the caller header correctly)
func (l *Meatlog) printfMessageWithCallerDepth(callerDepth int, severity uint8, format string, args ...any) {
	if l.minLogSeverityLevel <= severity {
		// This message is severe enough to log

		// if we have the SHORTFILE/LONGFILE flag set, we have to add a hack to ensure we get the file:line
		// of the CALLER of this function, otherwise the logger will always say the file:line of THIS
		// function, which is useless
		// the go builtin log library doesn't provide external options for controlling this
		if l.Flags()&(FLAG_LSHORTFILE|FLAG_LLONGFILE) != 0 {
			// remove flag before calling built-in log print functions so that it won't print the
			// file info, then we'll inject that information here and reenable the flag before returning
			// There is a race condition when two concurrent messages get logged and one of them
			// logs without file data at all. So we lock the meatlog, however, note that the underlying go log
			// calls will not respect this (i.e. meatlog.AsStdLog.Println() will ignore this lock and log anyway)
			// this won't cause panics/memory bugs/etc., so this should be fine, it just might make your log flags inconsistent
			// and the correct way to handle this is to not mix default logging unless necessary.

			// get the file information before we turn off the flags
			_, file, line, ok := runtime.Caller(callerDepth)
			if !ok {
				file = "???"
				line = 0
			}
			// this is the formatted log header of the file:line information (i.e. log.go:220)
			callerLocationString := l.FormatCallerLocation(file, line)
			// do a "write" lock, as we are changing flags and even normal log messages should pause until this is finished
			l.lock.Lock()
			// we'll need to set the flag back after the print function is called (so that the logger status is consistent)
			// so I don't think this defer won't be called if we panic or exit,
			// but that _should_ be okay as we'll be terminating the program and there shouldn't be any more logging after that.
			// if this proves NOT to be true, then we'll have to reset the flags and unlock in each of the os.exit and panic branches
			// and ensure teh l.lock.Unlock() doesn't get called twice (which is a panic :D)
			defer l.lock.Unlock()

			// turn off the flags, there's probably a fancier way involving bitwise operations that I'm too softbrained to think of right now
			if l.Flags()&FLAG_LSHORTFILE != 0 {
				l.SetFlags(l.Flags() - FLAG_LSHORTFILE)
				defer func() {
					l.SetFlags(l.Flags() | FLAG_LSHORTFILE)
				}()
			}
			if l.Flags()&FLAG_LLONGFILE != 0 {
				l.SetFlags(l.Flags() - FLAG_LLONGFILE)
				defer func() {
					l.SetFlags(l.Flags() | FLAG_LLONGFILE)
				}()
			}

			// now that we're safe to call the underlying go log methods, we need to inject the file information ourselves
			var newArgs []any
			newArgs = append(newArgs, callerLocationString)
			newArgs = append(newArgs, args...)
			args = newArgs
			//since this is a printf, we'll have to munge the format string so this extra arg is included in the format string
			format = "%s" + format

		} else {
			// if we don't have the SHORTFILE flag set, then we're not munging flags, so we don't need a "write" lock
			// attempt a blocking "read" lock. It is preferable for a log call to take longer than it is to fail to log at all
			l.lock.RLock()
			defer l.lock.RUnlock()
		}

		l.logger.Printf(format, args...)
		if l.exitOnSeverityLevel <= severity {
			// this message is severe enough to force an exit
			os.Exit(1)
		}
		if l.panicOnSeverityLevel <= severity {
			// this message is severe enough to force an panic
			panic("Panic called by logger due to message severity")
		}
	}
}

// PrintlnMessage sends a formatted log message at a given severity using fmt.PrintLn
func (l *Meatlog) PrintlnMessage(severity uint8, args ...any) {
	l.printlnMessageWithCallerDepth(2, severity, args...)
}

func (l *Meatlog) printlnMessageWithCallerDepth(callerDepth int, severity uint8, args ...any) {
	if l.minLogSeverityLevel <= severity {
		// This message is severe enough to log

		// if we have the SHORTFILE/LONGFILE flag set, we have to add a hack to ensure we get the file:line
		// of the CALLER of this function, otherwise the logger will always say the file:line of THIS
		// function, which is useless
		// the go builtin log library doesn't provide external options for controlling this
		if l.Flags()&(FLAG_LSHORTFILE|FLAG_LLONGFILE) != 0 {

			// remove flag before calling built-in log print functions so that it won't print the
			// file info, then we'll inject that information here and reenable the flag before returning
			// There is a race condition when two concurrent messages get logged and one of them
			// logs without file data at all. So we lock the meatlog, however, note that the underlying go log
			// calls will not respect this (i.e. meatlog.AsStdLog.Println() will ignore this lock and log anyway)
			// this won't cause panics/memory bugs/etc., so this should be fine, it just might make your log flags inconsistent
			// and the correct way to handle this is to not mix default logging unless necessary.

			// get the file information before we turn off the flags
			_, file, line, ok := runtime.Caller(callerDepth)
			if !ok {
				file = "???"
				line = 0
			}
			// this is the formatted log header of the file:line information (i.e. log.go:220)
			callerLocationString := l.FormatCallerLocation(file, line)
			// do a "write" lock, as we are changing flags and even normal log messages should pause until this is finished
			l.lock.Lock()
			// we'll need to set the flag back after the print function is called (so that the logger status is consistent)
			// so I don't think this defer won't be called if we panic or exit,
			// but that _should_ be okay as we'll be terminating the program and there shouldn't be any more logging after that.
			// if this proves NOT to be true, then we'll have to reset the flags and unlock in each of the os.exit and panic branches
			// and ensure teh l.lock.Unlock() doesn't get called twice (which is a panic :D)
			defer l.lock.Unlock()

			// turn off the flags, there's probably a fancier way involving bitwise operations that I'm too softbrained to think of right now
			if l.Flags()&FLAG_LSHORTFILE != 0 {
				l.SetFlags(l.Flags() - FLAG_LSHORTFILE)
				defer func() {
					l.SetFlags(l.Flags() | FLAG_LSHORTFILE)
				}()
			}
			if l.Flags()&FLAG_LLONGFILE != 0 {
				l.SetFlags(l.Flags() - FLAG_LLONGFILE)
				defer func() {
					l.SetFlags(l.Flags() | FLAG_LLONGFILE)
				}()
			}

			// now that we're safe to call the underlying go log methods, we need to inject the file information ourselves
			var newArgs []any
			newArgs = append(newArgs, callerLocationString)
			newArgs = append(newArgs, args...)
			args = newArgs

		} else {
			// if we don't have the SHORTFILE flag set, then we're not munging flags, so we don't need a "write" lock
			// attempt a blocking "read" lock. It is preferable for a log call to take longer than it is to fail to log at all
			l.lock.RLock()
			defer l.lock.RUnlock()
		}
		// actually make the log call to go log library
		l.logger.Println(args...)
		if l.exitOnSeverityLevel <= severity {
			// this message is severe enough to force an exit
			os.Exit(1)
		}
		if l.panicOnSeverityLevel <= severity {
			// this message is severe enough to force an panic
			panic("Panic called by logger due to message severity")
		}
	}
}

// ### Getters and Setters

// MinSeverityLevel returns the minimum severity level for a message to be logged
func (l *Meatlog) MinSeverityLevel() uint8 {
	return l.minLogSeverityLevel
}

// SetMinSeverityLevel set the minimum severity level for a message to be logged
func (l *Meatlog) SetMinSeverityLevel(severityLevel uint8) {
	l.minLogSeverityLevel = severityLevel
}

// ExitSeverityLevel returns the minimum severity level for a message to cause an immediate os.Exit()
func (l *Meatlog) ExitSeverityLevel() uint8 {
	return l.exitOnSeverityLevel
}

// SetExitSeverityLevel sets the minimum severity level for a message to cause an immediate os.Exit()
func (l *Meatlog) SetExitSeverityLevel(severityLevel uint8) {
	l.exitOnSeverityLevel = severityLevel
}

// PanicSeverityLevel returns the minimum severity level for a message to trigger a panic
func (l *Meatlog) PanicSeverityLevel() uint8 {
	return l.panicOnSeverityLevel
}

// SetPanicSeverityLevel returns the minimum severity level for a message to trigger a panic
func (l *Meatlog) SetPanicSeverityLevel(severityLevel uint8) {
	l.panicOnSeverityLevel = severityLevel
}

// ### functions for compatibility with underlying golang log module

// Println calls l.Output to print to the logger. Arguments are handled in the manner of fmt.Println.
// sent at INFO severity. Included for compatibility with golang built-in log module.
func (l *Meatlog) Println(args ...any) {
	l.printlnMessageWithCallerDepth(2, INFO_SEVERITY, args...)

}

// Print calls l.Output to print to the logger. Arguments are handled in the manner of fmt.Print.
// sent at INFO severity. Included for compatibility with golang built-in log module.
func (l *Meatlog) Print(args ...any) {
	l.printMessageWithCallerDepth(2, INFO_SEVERITY, args...)
}

// Print calls l.Output to print to the logger. Arguments are handled in the manner of fmt.Printf.
// sent at INFO severity. Included for compatibility with golang built-in log module.
func (l *Meatlog) Printf(format string, args ...any) {
	l.printfMessageWithCallerDepth(2, INFO_SEVERITY, format, args...)

}

// Flags returns the output flags for the underlying logger object
// maintains compatibility with golang built-in log module
func (l *Meatlog) Flags() int {
	return l.logger.Flags()
}

// Prefix returns the output prefix for the underlying logger object
// maintains compatibility with golang built-in log module
func (l *Meatlog) Prefix() string {
	return l.logger.Prefix()
}

// Output writes the output for a logging event.
// The string s contains the text to print after
// the prefix specified by the flags of the Logger.
// A newline is appended if the last character of s
// is not already a newline. Calldepth is used to
// recover the PC and is provided for generality,
// although at the moment on all pre-defined paths it will be 2.
// Maintains compatibility with golang built-in log module
func (l *Meatlog) Output(calldepth int, s string) error {
	return l.logger.Output(calldepth, s)
}

// SetOutput sets the output destination for the logger.
func (l *Meatlog) SetOutput(w io.Writer) {
	l.logger.SetOutput(w)
}

// SetOutput sets the output prefix string for the logger.
func (l *Meatlog) SetPrefix(s string) {
	l.logger.SetPrefix(s)
}

// SetFlags sets the output flags for the logger.
// The flag bits are Ldate, Ltime, and so on.
// Canonical flags are defined in the log package.
// Constants prefixed with FLAG_ are redefined in this module for convenience
func (l *Meatlog) SetFlags(flags int) {
	l.logger.SetFlags(flags)
}

// Writer returns the output destination for the logger.
func (l *Meatlog) Writer() io.Writer {
	return l.logger.Writer()
}

// ### These convenience functions log messages at predefined severity levels

// Trace() sends a trace-severity message (prefixed with "TRACE")
func (l *Meatlog) Trace(args ...any) {
	var arglist = []any{tracePrefix}
	arglist = append(arglist, args...)
	l.printMessageWithCallerDepth(3, TRACE_SEVERITY, arglist...)
}

// Tracef sends a formatted trace-severity message prefixed with "TRACE"
func (l *Meatlog) Tracef(format string, args ...any) {
	l.printfMessageWithCallerDepth(2, TRACE_SEVERITY, tracePrefix+format, args...)
}

// Debug() sends a debug-severity message (prefixed with "DEBUG")
func (l *Meatlog) Debug(args ...any) {
	var arglist = []any{debugPrefix}
	arglist = append(arglist, args...)
	l.printMessageWithCallerDepth(2, DEBUG_SEVERITY, arglist...)
}

// Debugf sends a formatted debug-severity message prefix with "DEBUG"
func (l *Meatlog) Debugf(format string, args ...any) {
	l.printfMessageWithCallerDepth(2, DEBUG_SEVERITY, debugPrefix+format, args...)
}

// Info sends an info-severity message prefixed with "INFO"
func (l *Meatlog) Info(args ...any) {
	var arglist = []any{infoPrefix}
	arglist = append(arglist, args...)
	l.printMessageWithCallerDepth(2, INFO_SEVERITY, arglist...)
}

// Infof sends a formatted info-severity message prefixed with "INFO"
func (l *Meatlog) Infof(format string, args ...any) {
	l.printfMessageWithCallerDepth(2, INFO_SEVERITY, infoPrefix+format, args...)
}

// Warn sends a warn-severity message prefixed with "WARN"
func (l *Meatlog) Warn(args ...any) {
	var arglist = []any{warnPrefix}
	arglist = append(arglist, args...)
	l.printMessageWithCallerDepth(2, WARN_SEVERITY, arglist...)
}

// Warnf sends a formatted warn-severity message prefixed with "WARN"
func (l *Meatlog) Warnf(format string, args ...any) {
	l.printfMessageWithCallerDepth(2, WARN_SEVERITY, warnPrefix+format, args...)
}

// Error sends an error-severity message prefixed with "ERROR"
func (l *Meatlog) Error(args ...any) {
	var arglist = []any{errorPrefix}
	arglist = append(arglist, args...)
	l.printMessageWithCallerDepth(2, ERROR_SEVERITY, arglist...)
}

// Errorf sends a formatted error-severity message prefixed with "ERROR"
func (l *Meatlog) Errorf(format string, args ...any) {
	l.printfMessageWithCallerDepth(2, ERROR_SEVERITY, errorPrefix+format, args...)
}

// Fatal sends a fatal-severity message prefixed with "FATAL"
func (l *Meatlog) Fatal(args ...any) {
	var arglist = []any{fatalPrefix}
	arglist = append(arglist, args...)
	l.printMessageWithCallerDepth(2, FATAL_SEVERITY, arglist...)
}

// Fatalf sends a formatted fatal-severity message prefixed with "FATAL"
func (l *Meatlog) Fatalf(format string, args ...any) {
	l.printfMessageWithCallerDepth(2, FATAL_SEVERITY, fatalPrefix+format, args...)
}

// Fatalln sends a fatal-severity message prefixed with "FATAL"
// provided for compatibility with golang log module
func (l *Meatlog) Fatalln(args ...any) {
	var arglist = []any{fatalPrefix}
	arglist = append(arglist, args...)
	l.printlnMessageWithCallerDepth(2, FATAL_SEVERITY, arglist...)
}

// Panicf prints a message prefixed with "PANIC" and calls panic()
// this is not sent at a severity level and is always logged
// used for compatibility with the golang built-in logger
func (l *Meatlog) Panic(args ...any) {
	var arglist = []any{panicPrefix}
	arglist = append(arglist, args...)
	// a panic message doesn't have a severity and is always send to the log
	l.logger.Panic(arglist...)
}

// Panicf prints a formatted message prefixed with "PANIC" and calls panic()
// this is not sent at a severity level and is always logged
// used for compatibility with the golang built-in logger
func (l *Meatlog) Panicf(format string, args ...any) {
	// a panic message doesn't have a severity and is always send to the log
	l.logger.Panicf(panicPrefix+format, args...)
}

// Panicln prints a message prefixed with "PANIC" and calls panic()
// this is not sent at a severity level and is always logged
// used for compatibility with the golang built-in logger
func (l *Meatlog) Panicln(args ...any) {
	var arglist = []any{panicPrefix}
	arglist = append(arglist, args...)
	// a panic message doesn't have a severity and is always send to the log
	l.logger.Panicln(arglist...)
}

// ### Convenience functions to log to the default logger

// Trace() logs a trace message to the default logger
func Trace(args ...any) {
	Default().Trace(args...)
}

// Debug() logs a trace message to the default logger
func Debug(args ...any) {
	Default().Debug(args...)
}

// Info() logs a trace message to the default logger
func Info(args ...any) {
	Default().Info(args...)
}

// Warn() logs a trace message to the default logger
func Warn(args ...any) {
	Default().Warn(args...)
}

// Error() logs a trace message to the default logger
func Error(args ...any) {
	Default().Error(args...)
}

// Fatal() logs a trace message to the default logger
func Fatal(args ...any) {
	Default().Fatal(args...)
}

// Trace() logs a trace message to the default logger
func Tracef(format string, args ...any) {
	Default().Tracef(format, args...)
}

// Debug() logs a trace message to the default logger
func Debugf(format string, args ...any) {
	Default().Debugf(format, args...)
}

// Info() logs a trace message to the default logger
func Infof(format string, args ...any) {
	Default().Infof(format, args...)
}

// Warn() logs a trace message to the default logger
func Warnf(format string, args ...any) {
	Default().Warnf(format, args...)
}

// Error() logs a trace message to the default logger
func Errorf(format string, args ...any) {
	Default().Errorf(format, args...)
}

// Fatal() logs a trace message to the default logger
func Fatalf(format string, args ...any) {
	Default().Fatalf(format, args...)
}
